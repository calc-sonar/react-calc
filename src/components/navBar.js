import React, { useState } from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';

function NavbarComponent() {

    const [color, setColor] = useState('primary');
    const [activeTag, setActiveTag] = useState('sum');

    return (
        <Navbar bg={color} expand="lg">
            <Container>
                <Navbar.Brand onClick={() => { setColor('primary') } }>
                    Calculadora React
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-4 me-auto">
                        <Nav.Link onClick={() => { setColor('primary'); setActiveTag("sum") } }>
                            <Link
                                to="/sum"
                                style={activeTag==="sum" ? {textDecoration: "none", color: "rgba(0,0,0,0.9)"} : {textDecoration: "none", color: "rgba(0,0,0,0.5)"}}
                            >
                                    Sumar
                            </Link>
                        </Nav.Link>
                        <Nav.Link onClick={() => { setColor('danger'); setActiveTag("resta") } }>
                            <Link
                                to="/resta"
                                style={activeTag==="resta" ? {textDecoration: "none", color: "rgba(0,0,0,0.9)"} : {textDecoration: "none", color: "rgba(0,0,0,0.5)"}}
                            >
                                    Restar
                            </Link>
                        </Nav.Link>
                        <Nav.Link onClick={() => { setColor('success'); setActiveTag("mult") } }>
                            <Link
                                to="/mult"
                                style={activeTag==="mult" ? {textDecoration: "none", color: "rgba(0,0,0,0.9)"} : {textDecoration: "none", color: "rgba(0,0,0,0.5)"}}
                            >
                                Multiplicar
                            </Link>
                        </Nav.Link>
                        <Nav.Link onClick={() => { setColor('warning'); setActiveTag("div") } }>
                            <Link
                                to="/div"
                                style={activeTag==="div" ? {textDecoration: "none", color: "rgba(0,0,0,0.9)"} : {textDecoration: "none", color: "rgba(0,0,0,0.5)"}}
                            >
                                Dividir
                            </Link>
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default NavbarComponent;