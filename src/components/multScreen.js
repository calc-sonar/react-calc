import React, {useState} from "react";
import { Stack, Button, Form, Card, Spinner, Container, Row, Col } from 'react-bootstrap';

const MultScreen = (props) => {
    const [data, setData] = useState({result: 0, status: "pending"});
    const [op1Internal, setOp1Internal] = useState(0);
    const [op2Internal, setOp2Internal] = useState(0);
    const [isSubmitting, setSubmitting] = useState(false);

    const axios = require('axios');

    async function getRest() {
        axios.get('https://quind-train-back.herokuapp.com/mult', {
            params: {
                op1: op1Internal,
                op2: op2Internal
                }
            })
            .then(function (response) {
                console.log(response);
                setData(response.data);
            })
            .catch(function (error) {
                console.log(error);
            })
            .then(function () {
                setSubmitting(false);
            });
    }

    return (
        <Container className="p-4">
            <Row>
                <Col xs={12} md={6} className="mb-4">
                    <Card>
                        <Card.Header>
                            <h5 className="mb-0">Multiplicar</h5>
                        </Card.Header>
                        <Card.Body>
                            <Form>
                                <Stack direction="vertical" gap="4">
                                    <Stack direction="horizontal" gap="3">
                                        <Form.Group controlId="op1">
                                            <Form.Label>Operador 1</Form.Label>
                                            <Form.Control
                                                type="number"
                                                placeholder="Ingresa un número"
                                                required
                                                value={op1Internal}
                                                onChange={e => setOp1Internal(e.target.value)}
                                            />
                                        </Form.Group>
                                        <Form.Text><h3 className="mb-0">*</h3></Form.Text>
                                        <Form.Group controlId="op2">
                                            <Form.Label>Operador 2</Form.Label>
                                            <Form.Control
                                                type="number"
                                                placeholder="Ingresa un número"
                                                required
                                                value={op2Internal}
                                                onChange={e => setOp2Internal(e.target.value)}
                                            />
                                        </Form.Group>
                                    </Stack>
                                    <Button
                                        variant="success"
                                        type="submit"
                                        disabled={isSubmitting}
                                        onClick={()=> { setSubmitting(true); getRest() }}>
                                        {isSubmitting ?
                                            <Stack direction="horizontal" gap="3" className="justify-content-center">
                                                <Spinner animation="border" role="status" size="sm">                                            
                                                </Spinner>
                                                <span>Multiplicando...</span>
                                            </Stack>
                                            :
                                            '¡Multiplicar!'}
                                    </Button>
                                </Stack>
                            </Form>
                        </Card.Body>
                    </Card>
                </Col>
                
                <Col xs={12} md={6} className="mb-4">
                    <Card bg={data.status==="pending"? "secondary" : data.status==="Ok"? "success" : "danger" }>
                        <Card.Header>
                            <h5 className="mb-0" style={{color: "white"}}>Resultado</h5>
                        </Card.Header>
                        <Card.Body>
                            {
                                data.status==="pending"?
                                <h4>Aún no se realiza una multiplicación</h4> : 
                                data.status==="Ok"?
                                <Stack className="justify-content-center" direction="horizontal" gap="2" style={{color: "white"}}>
                                    <h4>{op1Internal || 0}</h4>
                                    <h4>*</h4>
                                    <h4>{op2Internal || 0}</h4>
                                    <h4>=</h4>
                                    <h4>{data.result || 0}</h4>
                                </Stack> :
                                <h4>{data.status}</h4>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}

export default MultScreen;