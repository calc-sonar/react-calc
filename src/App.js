import './App.css';
import NavbarComponent from './components/navBar';
import { Routes, Route } from 'react-router';
import SumaScreen from './components/sumaScreen';
import RestaScreen from './components/restaScreen';
import MultScreen from './components/multScreen';
import DivScreen from './components/divScreen';

function App() {
  return (
    <div className="App">
      <NavbarComponent />
      <Routes>
        <Route path="/" element={<SumaScreen />} />
        <Route path="/sum" element={<SumaScreen />} />
        <Route path="/resta" element={<RestaScreen />} />
        <Route path="/mult" element={<MultScreen />} />
        <Route path="/div" element={<DivScreen />} />
      </Routes>
    </div>
  );
}

export default App;
