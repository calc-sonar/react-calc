FROM node:lts-alpine3.15

RUN mkdir -p /usr/src/app

#ADD ./ /usr/src/app

ADD ./public /usr/src/app
ADD ./src /usr/src/app
ADD package.json /usr/src/app
ADD package-lock.json /usr/src/app

WORKDIR /usr/src/app /usr/src/app

RUN npm install

CMD ["npm", "start"]
EXPOSE 3000
